The idea of this application is to let someone register. Thus enabling them to pose a question about a workshop, conference or presentation that was giving @ the Creative Gym. Other participants can then answer the question and ask there own questions.

The application is kind of like of forum.

Some of the features for this app are:

* registration and login (via social media and local login/registration)
* via geolocation the user gets to see the topics/discussions in his neighborhood
* the moderator (which is the author of a topic) is able to close a topic or message or even delete it

The tecnical requirements for this project are:

* css animations
* socket.io
* mongoDB
* sass
* nodeJS
* express framework
* gulp


To see this application live visit [Live QA App](http://qa-forum.herokuapp.com).