var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');
var session = require('express-session');
var configDB = require('./config/database.js');

//Routes
var routes = require('./routes/index');
/*var users = require('./routes/users');*/
var newTopic = require('./routes/newtopic');
var topicDetail = require('./routes/topicdetail');
var profile = require('./routes/profile');
var profileFacebook = require('./routes/profilefacebook');
var profileTwitter = require('./routes/profiletwitter');
var myTopics = require('./routes/mytopics');
/*var signup = require('./routes/signup');
var signin = require('./routes/signin');*/


var app = express();

// MongoDB
mongoose.connect(configDB.url); // connection to the db

require('./config/passport')(passport); // pass passport for configuration

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Passport
app.use(session({ secret: 'webtech2' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

require('./app/routes.js')(app, passport); // load in all the routes and pass in our app and fully configured passport

app.use('/', routes);                                            
/*app.use('/users', users);*/
app.use('/newtopic', newTopic);
app.use('/topicdetail', topicDetail);
app.use('/profile', profile);
app.use('/profilefacebook', profileFacebook);
app.use('/profiletwitter', profileTwitter);
app.use('/mytopics', myTopics);
/*app.use('/signup', signup);
app.use('/signin', signin);*/


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
