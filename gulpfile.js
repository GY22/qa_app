// load gulp
var gulp = require('gulp');
var concatCss = require('gulp-concat-css');
var minifyCss = require('gulp-minify-css');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var nodemon = require('gulp-nodemon');


gulp.task('styles', function (){
	gulp.src('public/stylesheets/css/*.css')
    .pipe(concatCss('build.css'))
    .pipe(minifyCss({keepBreaks:true}))
    .pipe(gulp.dest('public/build/css/'))
});

gulp.task('scripts', function() {
  gulp.src('public/javascripts/livereload.js')
    .pipe(concat('livereload.js'))
    .pipe(uglify())
    .pipe(gulp.dest('public/build/js/'));
});

gulp.task('watch', function (){
	gulp.watch('public/stylesheets/css/*.css', ['styles']);
	gulp.watch('public/javascripts/*.js', ['scripts']);
});

gulp.task('develop', function () {
  nodemon({ script: 'bin/www', ext: 'ejs js', ignore: ['ignored.js'] })
    .on('restart', function () 
    {
      console.log('Restarted')
    })
})

gulp.task('default', ['styles', 'scripts' ,'watch', 'develop']);