var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//var Schema = mongoose.Schema, ObjectId = Schema.ObjectId;

var commentSchema = new Schema({
	//commentID:           ObjectId,
  commentBody: 	       String,
	commentDateCreated: { 
    type: Date, 
    default: Date.now 
  },
  fbId: 				     String,
  twId: 				     String,
  fbUsername:			   String,
  twUsername:			   String,
  localUsername:     String,
  commentTopicId:    String
  
  //base64Image:       String
  //profilePic 

});

var Comment = mongoose.model('Comment', commentSchema);

module.exports = Comment;

///test