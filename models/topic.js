var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var topicSchema = new Schema ({
  topicTitle: 			    String,
  topicDescription: 	  String,
  topicDateCreated: { 
    type: Date, 
    default: Date.now 
  },
  fbId: 				        String,
  twId: 				        String,
  fbUsername:           String,
  twUsername:           String,
  localUsername:        String,
  location: {
    type: {
      type : String,
      default: "Point"
    },
    coordinates: [ ]
  }
});

topicSchema.index({ location: '2dsphere' });
var Topic = mongoose.model('Topic', topicSchema);

module.exports = Topic;