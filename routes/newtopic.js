var mongoose = require('mongoose');
var Topic = require('../models/topic');
var db = require('../config/database');
var express = require('express');
var router = express.Router();

// render the start/create a new topic view
router.get('/', function(req, res) {
  res.render('newtopic');
});

// save topic to db
router.post('/',function(req, res, next) {
	
	console.log('The post was submitted'); 

/*	var coordinateArray = req.body.coordinates.split(",");
	console.log("Logging array req.body.coordinates");
	console.log(coordinateArray)*/;

	var convert = req.body.coordinates;
	/*console.log("Logging convert");
	console.log(convert);*/

	var split = convert.split(",");
	console.log("Logging split");
	console.log(split);

	var x = split[0];
	var y = split[1];
	console.log("Logging var LON " + x);
	console.log("Logging var LAT " + y);

	var point = { type: "Point", coordinates: [ x, y ] };	
	console.log("Logging point");
	console.log(point);

	console.log("xxxxxx");
	console.log( x, y ); 

	var topic = new Topic
	({
		"topicTitle": 		req.body.topicTitle,
		"topicDescription": req.body.topicDescription,
		"fbId": 			req.body.userIdFB,
		"twId": 			req.body.userIdTW,
		"fbUsername": 		req.body.usernameFB,
		"twUsername": 		req.body.usernameTW,
		"localUsername": 	req.body.localUsername,
		"location":     	point
	});

	console.log("test here ");
	console.log(req.body.coordinates);
	
	topic.save(function (err, topic)
	{
		if(err){
			return next(err)
			console.log('Failed to save the topic to the database');
		}
		else
		{
			console.log('Saved the topic succesfully to the database');
			// each topic has its own unique url 
			res.redirect('/topicdetail/' + topic._id);
		}
	})

});

module.exports = router;
