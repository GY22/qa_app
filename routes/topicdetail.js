var mongoose = require('mongoose');
var Topic = require('../models/topic');
var Comment = require('../models/comments');
var db = require('../config/database');
var express = require('express');
var router = express.Router();

/*router.get('/', function(req, res, next) {
  res.render('topicdetail');
});*/

// shows the view of one unique topic based on id
router.get('/:id', function(req, res, next) {
  
  var topicId = req.params.id;
  console.log('Logging topic id: ' + topicId);

  // find one single topic based on id
  Topic.findById(topicId, function(err, topic)
  {
  	if(err)
    {
  		console.log('There was no topic with this ID');
      return next(err)
  	}
  	else
  	{
  		console.log('Whoop whoop we found a topic matching the requested ID');
  		/*//show the data on the page
  		res.render('topicdetail', { topic: topic } );

  		console.log("Logging data: " + topic);
  		console.log("Loggin data title out db: " + topic.topicTitle);
  		console.log("Loggin data desc out db: " + topic.topicDescription);
  		console.log("Loggin data date out db: " + topic.topicDateCreated);*/

      Comment.find({commentTopicId: topic._id}).exec(function(err, comments) 
      {
          //renders the data on the page
          res.render('topicdetail', { topic: topic , comments: comments } );

          console.log("Logging data topic: " + topic);
          console.log("Loggin data title out db: " + topic.topicTitle);
          console.log("Logging data comment: " + comments);
          console.log("Logging error: " + err);
      });
  	}
  });

});

module.exports = router;
